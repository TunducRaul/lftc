import Algorithms.First;
import Algorithms.Follow;
import Models.Grammar;
import Utilities.GrammarParser;

import java.util.HashMap;

public class Main {

    public static void main(String[] args) {
        GrammarParser parser = new GrammarParser("grammar.txt");
        Grammar grammar = parser.getGrammar();
        System.out.println(grammar);

        First first = new First();
        first.initializeFirst();
        first.computeFirst();
        System.out.println(first);

        Follow follow = new Follow.Builder()
                .grammar(grammar)
                .first(first)
                .follow(new HashMap<>())
                .build();
        System.out.println(follow);
    }
}