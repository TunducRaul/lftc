package Models;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Alexandra Muresan on 05-Dec-17.
 */
public class Production {

    private String initialState;
    private List<String> transitions;

    public Production(String initialState) {
        this.initialState = initialState;
        transitions = new ArrayList<>();
    }

    public void addTransition(String transition) {
        transitions.add(transition);
    }

    public String getInitialState() {
        return initialState;
    }

    public List<String> getTransitions() {
        return transitions;
    }

    @Override
    public String toString() {
        String result = initialState + "->";
        for(int i=0;i<transitions.size()-1;i++){
            result += transitions.get(i) + "|";
        }
        result += transitions.get(transitions.size()-1);
        return result;
    }
}
